import Manager.Manager;
import room.Room;
import toys.*;
import types.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) throws IOException {

        Toy ball1 = new Ball(10, "white", BallTypes.BallType.football);
        Toy ball2 = new Ball(12, "red", BallTypes.BallType.regularball);
        Toy car1 = new Car(5, "green", CarTypes.CarType.meddium);
        Toy car2 = new Car(7, "blue", CarTypes.CarType.big);
        Toy doll1 = new Doll(12, "white", "blonde", Genders.Gender.female);
        Toy doll2 = new Doll(12, "white", "red", Genders.Gender.female);
        Toy doll3 = new Doll(12, "white", "blonde", Genders.Gender.male);

        List<Toy> toys = new ArrayList<Toy>();

        toys.add(ball1);
        toys.add(ball1);
        toys.add(car2);
        toys.add(ball2);
        toys.add(car1);

        Room room1 = new Room(AgeGroup.Age.child, toys);
        List<Toy> toys1 = new ArrayList<Toy>();

        toys1.add(doll1);
        toys1.add(doll2);
        toys1.add(doll3);
        toys1.add(doll2);

        Room room2 = new Room(AgeGroup.Age.preschooler, toys1);
        List<Toy> toys2 = new ArrayList<Toy>();

        Toy cube1 = new Cube(3, "black", CubeTypes.CubeType.images, 5);
        Toy cube2 = new Cube(6, "red", CubeTypes.CubeType.letters, 7);
        toys2.add(cube1);
        toys2.add(cube2);
        toys2.add(cube2);

        Room room3 = new Room(AgeGroup.Age.toodler, toys2);

        List<Room> rooms = new ArrayList<Room>();
        rooms.add(room1);
        rooms.add(room2);
        rooms.add(room3);

        Manager manager = new Manager(rooms);
        manager.start();

    }
}
