package Manager;

import room.Room;
import toys.Toy;
import types.AgeGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Manager {
    private List<Room> rooms;

    public Manager(List<Room> _rooms) {
        rooms = _rooms;
    }

    public String toString() {
        String format = "";
        for (int i = 0; i < rooms.size(); ++i) {
            format += "Room #" + i + ": \n" + rooms.get(i).toString();
        }
        return format;
    }

    public List<Room> filterByGroup(final AgeGroup.Age groupName) {
        final List<Room> filtered = new ArrayList<Room>();
        rooms.stream().filter((room) -> (room.getAgeGroup() == groupName))
            .forEachOrdered((room) -> {
                filtered.add(room);
            });
        return filtered;
    }

    public List<Room> filterByBudget(final int budget) {
        final List<Room> filtered = new ArrayList<Room>();
        rooms.stream().filter((room) -> (room.getBudget() <= budget)).forEachOrdered((room) -> {
            filtered.add(room);
        });
        return filtered;
    }


    public void start() throws IOException {
        int selector;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Your options:\n1:Show all rooms\n2:Filter rooms by budget\n"
            + "3:Filter room by Age group\n4:Exit\nPlease enter number to chose: ");
        selector = Integer.parseInt(br.readLine());

        switch (selector) {
            case 1:
                roomOptions(rooms);
                break;
            case 2:
                System.out.print("Enter budget:\n");
                roomOptions(filterByBudget(Integer.parseInt(br.readLine())));
                break;
            case 3:
                System.out.print("Select age group");
                AgeGroup.Age age = selectAgeGroup();
                roomOptions(filterByGroup(age));
                break;
            case 4:
                break;
            default:
                break;
        }
    }

    private void roomOptions(List<Room> filteredRooms) throws IOException {
        int selector;

        for (Room room : filteredRooms) {
            System.out.print("Room " + (rooms.indexOf(room) + 1) + "\n" + room.toString());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("\nYour options:\n1:Select room\n2:Return to main menu\n3:Exit"
            + "\nPlease enter number to chose: ");

        selector = Integer.parseInt(br.readLine());

        switch (selector) {
            case 1:
                System.out.print("Enter number of room: ");
                int selectedRoom = Integer.parseInt(br.readLine());
                if (selectedRoom > rooms.size()) {
                    System.out.print("\nNo such room.\n");
                    roomOptions(filteredRooms);
                    break;
                }
                System.out.print("\nYou selected " + (selectedRoom) + " room:\n" + filteredRooms
                    .get(selectedRoom - 1).toString() + "\nPress any Enter to continue\n");
                br.readLine();
                roomOptions(filteredRooms);
                break;
            case 2:
                start();
                break;
            case 3:
                break;
            default:
                break;
        }
    }

    private AgeGroup.Age selectAgeGroup() throws IOException {
        int selector;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out
            .print("\n1: Toddler\n2: Kindergarten Child\n3: Preschooler\nEnter number to select:");
        selector = Integer.parseInt(br.readLine());
        switch (selector) {
            case 1:
                return AgeGroup.Age.toodler;
            case 2:
                return AgeGroup.Age.child;
            case 3:
                return AgeGroup.Age.preschooler;
            default:
                break;
        }
        return null;
    }
}
