package toys;

public abstract class Toy {
    private int price;
    private String groupName;
    private String color;

    public Toy(int _price, String _color) {
        price = _price;
        color = _color;
        groupName = this.getClass().toString();
    }

    public int getPrice() {
        return price;
    }

    public String getGroupName() {
        groupName = groupName.substring(groupName.indexOf('.')+1);
        return groupName;
    }

    public String getColor() {
        return color;
    }

    public abstract String toString();
}
